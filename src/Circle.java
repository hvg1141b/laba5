/*
 *
 */

/**
 *
 * @author Виктор
 */
class Circle {

    // свойства класса
    public double x; // абсцисса центра
    public double y; // ордината центра
    public double r; // радиус
    // методы класса     
        
    public void printLongCircle() {
        System.out.println("Длина окружности=" + (2 * Math.PI * r));
    }

    // перемещает центр, движение окружности
    public void moveCircle() {
        x = (int) (x + Math.random() * 198 - 99);
        y = (int) (y + Math.random() * 198 - 99);
    }
// конструктор по умолчанию, теперь сразу после создания объекта будем получать окружность
// единичного радиуса с центром в начале координат

    public static void printxy(Circle o1) {
        System.out.println("x=" + (o1.x));
        System.out.println("y=" + (o1.y));
        System.out.println("r=" + (o1.r));
    }
    
    public Circle() {
        this.x = 0.0;
        this.y = 0.0;
        this.r = 1.0;
    }

    public Circle(double a, double b, double c) {
        this.x = a;
        this.y = b;
        this.r = c;
    }

    // расстояние между центрами двух окружностей
    public double DistanceCircle(Circle o1, Circle o2) {
         return (Math.sqrt(Math.pow(o2.x - o1.x, 2) + Math.pow(o2.y - o1.y, 2)));
    }
    
    public boolean TouchCircle(Circle cir) {
        if ((this.r + cir.r) == (DistanceCircle(this, cir))) {
           return true;
        } else {
            if (this.r > cir.r) {
                if ((this.r - cir.r) == DistanceCircle(this, cir)) {
                    return true;
                } else {
                    if ((cir.r - this.r) == DistanceCircle(this, cir)) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }
        return false;
    }
}